import Layout from '../../src/components/layout/layout'
import { SignInForm } from '../../src/components/user/SignInForm'

interface Props {}

export default function SignIn (props: Props) {
  return (
    <Layout pageTitle='Sign In'>
      <SignInForm />
    </Layout>
  )
}
