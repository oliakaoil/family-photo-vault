import { NextApiRequest, NextApiResponse } from 'next';
import { UserService } from '../../src/services/user.service';
import { ApiMiddleware, IApiMiddleware } from '../../src/api.middleware';
import { session } from 'next-session';
import { Session } from 'next-session/dist/types';
import nc from "next-connect";

const userService = new UserService();
const middleware: IApiMiddleware = new ApiMiddleware();

type NextApiRequestWithSession = NextApiRequest & {
    session: Session;
};

const SignIn = async (req: NextApiRequestWithSession, res: NextApiResponse) => {
    const email = req.body.email;
    const password = req.body.password;

    if (!email || !password) {
        res.status(404).send(null);
        return;
    }

    const user = userService.signIn(email, password);

    if (!user) {
        res.json({ error: 'That login was not found' });
        return;
    }

    req.session.user = user;
    res.json({ data: user });
};

const SignOut = async (req: NextApiRequestWithSession, res: NextApiResponse) => {
    delete req.session.user;
    res.json(0);
};

const GetSessionUser = async (req: NextApiRequestWithSession, res: NextApiResponse) => {
    const signedIn = (req.session && req.session.user);
    res.json({ data: signedIn ? req.session.user : null });
};

const apiRoute = nc()
    .use(session())
    .all(async (req: NextApiRequestWithSession, res: NextApiResponse) => {

        await middleware.all(req, res);

        switch (req.method) {
            case 'POST':
                await SignIn(req, res);
                return;
            case 'GET':
                await GetSessionUser(req, res);
                return;
            case 'DELETE':
                await SignOut(req, res);
                return;
            default:
                res.status(404).send(null);
                return;
        }
    });

export default apiRoute;