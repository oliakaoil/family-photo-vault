import { NextApiRequest, NextApiResponse } from "next";
import { PhotoService } from "../../src/services/photo.service";
import { ApiMiddleware, IApiMiddleware } from "../../src/api.middleware";
import { session } from "next-session";
import { Session } from "next-session/dist/types";
import * as path from "path";
import * as fs from "fs";
import multer from "multer";
import nc from "next-connect";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

const photoService = new PhotoService();
const middleware: IApiMiddleware = new ApiMiddleware();

type NextApiRequestWithSession = NextApiRequest & {
  session: Session;
};

const GetPhotos = async (req: NextApiRequest, res: NextApiResponse) => {
  const limit = Number(req.query.limit ?? 100);
  const offset = Number(req.query.offset ?? 0);
  return photoService.getAll(offset, limit).then(res.json);
};

const StreamPhoto = async (req: NextApiRequest, res: NextApiResponse) => {
  const photoId: string = String(req.query.i);
  const thumb: boolean = req.query.thumb === "1";
  const photo = await photoService.getById(photoId);

  if (!photo) {
    res.status(404).send(null);
    return;
  }

  // {
  //   id: '516d580429877ca48c2c0dc23213a70e',
  //   filename: 'cd2a742872a04721945ea7e52b000aca.jpg',
  //   filesize: 400404,
  //   created: '2021-02-14T02:47:46.000Z'
  // }
  let photoFilepath: string = "";

  if (thumb) {
    photoFilepath = photoService.getFilepath(photo, true);
  } else {
    photoFilepath = photoService.getFilepath(photo);
  }
  const photoExists = await fs.existsSync(photoFilepath);

  console.log(photoFilepath);

  if (!photoExists) {
    console.error(`missing photo ${photoFilepath}`);
    res.status(404).send(null);
    return;
  }

  res.setHeader("content-type", "image/jpeg");
  fs.createReadStream(photoFilepath).pipe(res);
};

const GetPhoto = async (req: NextApiRequest, res: NextApiResponse) => {
  const photoId: string = String(req.query.i);
  const photo = await photoService.getById(photoId);

  if (!photo) {
    res.status(404).send(null);
    return;
  }

  return res.json(photo);
};

const AddPhoto = async (req: NextApiRequest, res: NextApiResponse) => {
  const { file = {} } = req as any;

  const fileAdded = await photoService.add(file.path);

  if (!fileAdded) {
    return res.status(500).send(0);
  }

  return res.json(0);
};

const RotatePhoto = async (req: NextApiRequest, res: NextApiResponse) => {
  const photoId: string = String(req.query.i);
  const direction: string = String(req.query.d);
  const photo = await photoService.getById(photoId);

  if (!photo) {
    res.status(404).send(null);
    return;
  }

  const photoRotated = await photoService.rotate(photo, direction);

  if (!photoRotated) {
    return res.status(500).send(0);
  }

  return res.json(0);
};

const upload = multer({
  storage: multer.diskStorage({
    destination: path.join(publicRuntimeConfig.PHOTO_FILEPATH, "uploads"),
    filename: (req: any, file: any, cb: CallableFunction) =>
      cb(null, file.originalname),
  }),
});

const apiRoute = nc()
  .use(upload.single("photo"))
  .use(session())
  .all(async (req: NextApiRequestWithSession, res: NextApiResponse) => {
    await middleware.all(req, res);

    const { session, query } = req;

    if (!session?.user) {
      res.status(403).send(0);
      return;
    }

    switch (req.method) {
      case "GET":
        if (query?.i) {
          if (query?.rotate) {
            await RotatePhoto(req, res);
            return;
          }
          if (query?.stream) {
            await StreamPhoto(req, res);
            return;
          }

          await GetPhoto(req, res);
          return;
        }

        await GetPhotos(req, res);
        return;

      case "POST":
        await AddPhoto(req, res);
        return;
      default:
        res.status(404);
        return;
    }
  });

export const config = {
  api: {
    bodyParser: false, // Disallow body parsing, consume as stream
  },
};

export default apiRoute;
