import { AppProps } from "next/app";
import SnackbarProvider from "react-simple-snackbar";
import "../node_modules/tachyons/css/tachyons.css";
import "../node_modules/react-datepicker/dist/react-datepicker.css";
import "../scss/app.scss";

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <SnackbarProvider>
      <Component {...pageProps} />
    </SnackbarProvider>
  );
}
