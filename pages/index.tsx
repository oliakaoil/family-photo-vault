import Layout from "../src/components/layout/layout";
import { PhotoFeed } from "../src/components/photos/PhotoFeed";

interface Props {
  isAdmin: boolean;
}

export default function Home(props: Props) {
  const { isAdmin } = props;
  return (
    <Layout pageTitle="All Photos">
      <PhotoFeed isAdmin={isAdmin} />
    </Layout>
  );
}
