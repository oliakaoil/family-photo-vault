import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Router from "next/router";
import Layout from "../../../src/components/layout/layout";
import PhotoPage from "../../../src/components/photos/PhotoPage";
import PhotoModel from "../../../src/models/photo";

interface Props {
  isAdmin: boolean;
}

export default function PhotoView(props: Props) {
  const { id } = useRouter().query;
  const [photo, setPhoto] = useState<PhotoModel | null>(null);
  const { isAdmin } = props;

  useEffect(() => {
    fetch(`/api/photos?i=${id}`)
      .then((response) => {
        if (response.status === 403) {
          Router.push("/user/sign-in");
          return;
        }

        return response.json();
      })
      .then(setPhoto);
  }, []);

  if (!photo) {
    return null;
  }

  return (
    <Layout pageTitle="View Photo">
      <div>{id && <PhotoPage photo={photo} isAdmin={isAdmin} />}</div>
    </Layout>
  );
}
