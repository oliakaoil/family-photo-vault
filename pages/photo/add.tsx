import React, { useState } from "react";
import Dropzone from "react-dropzone";
import DatePicker from "react-datepicker";
import request from "superagent";
import Layout from "../../src/components/layout/layout";
import { useSnackbar } from "react-simple-snackbar";
import { ErrorOpts, SuccessOpts } from "../../src/snackbar-options";

interface Props {}

export default function Add(props: Props) {
  const [photoDate, setPhotoDate] = useState<Date>(new Date());
  const [file, setFile] = useState<any>();

  const [errorSnackbar] = useSnackbar(ErrorOpts);
  const [successSnackbar] = useSnackbar(SuccessOpts);

  const handleAddPhoto = () => {
    if (!file) {
      return;
    }

    request
      .post("/api/photos")
      .attach("photo", file)
      .end((err: any, res: any) => {
        if (err) {
          console.error(err);
          errorSnackbar(
            "Whoops..that didn't work :-(. Who built this website anyways?"
          );
        }

        setFile(null);
        successSnackbar("The photo was added");
      });
  };

  const hasFile = Boolean(file);

  return (
    <Layout pageTitle="Add Photo">
      <div>
        <div className="b mb4 f3">Add a Photo</div>

        <div
          className="ba bg-moon-gray white b--dashed pa3 w-100 w-50-ns pointer"
          style={{ height: 120 }}
        >
          <Dropzone
            onDrop={(acceptedFiles: any[]) => setFile(acceptedFiles.shift())}
            accept="image/jpeg, image/png, image/gif"
          >
            {({ getRootProps, getInputProps }) => (
              <section className="w-100 h-100">
                <div {...getRootProps()} className="w-100 h-100">
                  <input {...getInputProps()} />
                  {hasFile && (
                    <div
                      className="blue"
                      style={{ outline: "none !important" }}
                    >
                      {file.name} ({Math.floor(file.size / 1000)}kb)
                    </div>
                  )}
                  {!hasFile && (
                    <div>
                      Drag and drop a file here, or click to select a file
                    </div>
                  )}
                </div>
              </section>
            )}
          </Dropzone>
        </div>

        <div className="mt4">
          <span className="dib mr2 b">Photo Date:</span>
          <DatePicker
            selected={photoDate}
            onChange={(val: Date) => {
              console.log(val);
              setPhotoDate(val);
            }}
          />
        </div>

        <div className="mt4">
          <span
            className={`button primary ${file ? "" : "disabled"}`}
            onClick={handleAddPhoto}
          >
            Add Photo
          </span>
        </div>
      </div>
    </Layout>
  );
}
