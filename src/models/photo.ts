export default interface PhotoModel {
    id: string;
    filename: string;
    created: string;
    filesize: number;
}