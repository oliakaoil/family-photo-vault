import moment from "moment";
import * as path from "path";
import * as fs from "fs";
import sharp from "sharp";
import md5 from "md5";
import uniqid from "uniqid";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

export class Photo {
  public id: string;
  public filename: string;
  public filesize: number;
  public created: string;

  constructor(o?: any) {
    this.id = o ? o.id : null;
    this.filename = o ? o.filename : null;
    this.filesize = o ? o.filesize : null;
    this.created = o ? o.created : null;
  }
}

export interface IPhotoService {
  add(filepath: string): Promise<boolean>;

  getAll(offset: number, limit: number, sort: string): Promise<Photo[]>;

  getById(id: string): Promise<Photo | null>;

  getFilepath(photo: Photo, thumb?: boolean): string;

  rotate(photo: Photo, direction?: string): Promise<boolean>;
}

export class PhotoService implements IPhotoService {
  public async add(filepath: string): Promise<boolean> {
    const stat = await fs.statSync(filepath);

    if (!stat?.size) {
      return false;
    }

    const ext = String(filepath.split(".").pop()).toLowerCase();
    const photoId = md5(`${uniqid()}${new Date().getTime()}`);
    const thumbFilename = `${photoId}-thumb.${ext}`;
    const largeFilename = `${photoId}.${ext}`;

    const thumbFilepath = path.join(
      publicRuntimeConfig.PHOTO_FILEPATH,
      "src",
      thumbFilename
    );
    const largeFilepath = path.join(
      publicRuntimeConfig.PHOTO_FILEPATH,
      "src",
      largeFilename
    );

    const sourceFileBuf = await fs.readFileSync(filepath);

    // thumbnail
    try {
      await sharp(sourceFileBuf).resize(200, 200).toFile(thumbFilepath);
    } catch (err) {
      console.error(err);
      return false;
    }

    // large size
    try {
      await sharp(sourceFileBuf).resize(1280, 1024).toFile(largeFilepath);
    } catch (err) {
      console.error(err);
      return false;
    }

    const newPhoto = new Photo({
      id: photoId,
      filename: largeFilename,
      filesize: stat.size,
      created: new Date(),
    });

    const photos = await this.getDb();
    photos.push(newPhoto);

    photos.sort((a, b) => {
      const aComp = a.created;
      const bComp = b.created;
      if (aComp === bComp) {
        return 0;
      }
      return aComp > bComp ? -1 : 1;
    });

    try {
      await fs.writeFileSync(
        publicRuntimeConfig.PHOTODB_FILEPATH,
        JSON.stringify({ photos }, null, 2)
      );
    } catch (err) {
      console.error(err);
      return false;
    }

    return true;
  }

  public async getAll(
    offset: number,
    limit: number,
    sort?: string
  ): Promise<Photo[]> {
    const photos = await this.getDb();

    const sortedPhotos = photos.sort((a: Photo, b: Photo) => {
      const aVal = a.created;
      const bVal = b.created;

      if (aVal === bVal) {
        return 0;
      }

      return moment(aVal).isBefore(moment(bVal)) ? 1 : -1;
    });

    const pagedPhotos = sortedPhotos.slice(offset, offset + limit);

    return pagedPhotos;
  }

  public async getById(id: string): Promise<Photo | null> {
    const photos = await this.getDb();
    const photo = photos.find((photo: Photo) => photo.id === id);
    return photo ? (photo as Photo) : null;
  }

  public getFilepath(photo: Photo, thumb?: boolean): string {
    let { filename } = photo;

    if (thumb) {
      const parts = filename.split(".");
      const ext = parts.pop();
      filename = `${parts.join(".")}-thumb.${ext}`;
    }

    return path.join(process.cwd(), "photos", "src", filename);
  }

  public async rotate(photo: Photo, direction?: string): Promise<boolean> {
    const filepath = this.getFilepath(photo);
    const largeFileBuf = await fs.readFileSync(filepath);
    const rotateAngle = 90;

    try {
      await sharp(largeFileBuf).rotate(rotateAngle).toFile(filepath);
    } catch (err) {
      console.error(err);
      return false;
    }

    const thumbFilepath = this.getFilepath(photo, true);
    const thumbFileBuf = await fs.readFileSync(thumbFilepath);

    try {
      await sharp(thumbFileBuf).rotate(rotateAngle).toFile(thumbFilepath);
    } catch (err) {
      console.error(err);
      return false;
    }

    return true;
  }

  private async getDb(): Promise<Photo[]> {
    try {
      const raw = (
        await fs.readFileSync(publicRuntimeConfig.PHOTODB_FILEPATH)
      ).toString();
      const data = JSON.parse(raw);
      return data.photos as Photo[];
    } catch (err) {
      console.error(err);
      return [];
    }
  }
}
