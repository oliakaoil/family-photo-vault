import { UserService, IUserService, User } from './user.service';

let userService: IUserService;
let user1: User;

beforeEach(() => {
    userService = new UserService();

    user1 = new User();
    user1.email = 'test-user1@test.net';
    user1.name = 'Test User';
    user1.password = 'password';    
});


test('Signing in with valid email returns user', () => {

    userService.getByEmail = jest.fn(email => user1);
        
    const response = userService.signIn(user1.email, user1.password);

    expect(userService.getByEmail.mock.calls.length).toBe(1);
    expect(response).toBe(user1);
});