import * as UserDb from '../../data/userdb.json';

export class User {
    public name: string;
    public email: string;
    public isAdmin?: boolean;

    constructor(name: string, email: string) {
        this.name = name;
        this.email = email;
    }
};

export interface IUserService {

    getByEmail(email: string): User|undefined;
    
    signIn(email: string, password: string): User|undefined;

    signedIn(): boolean;

    signOut(): boolean;
}

export class UserService implements IUserService {

    public getByEmail(email: string): User|undefined
    {
        return UserDb.users.find((user: User) => user.email === email);
    }

    public signIn(email: string, password: string): User|undefined
    {
        const user = this.getByEmail(email);
        if (!user) {
            return;
        }

        if (password !== UserDb.password) {
            return;
        }

        user.isAdmin = (UserDb.admins.indexOf(user.email) > -1);

        return user;
    }

    public signedIn(): boolean
    {
        return true;
    }

    public signOut(): boolean
    {
        return true;
    }    
}