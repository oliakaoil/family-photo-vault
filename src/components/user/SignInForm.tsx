import React from 'react'
import { Button } from '../common/Button'
import Router from 'next/router'

interface Props {}

interface State {
  email: string
  password: string
  error: string
}

export class SignInForm extends React.Component<Props, State> {
  private emailInputRef: any

  constructor (props: Props) {
    super(props)

    this.emailInputRef = React.createRef()
    this.handleChangeEmail = this.handleChangeEmail.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.handleClick = this.handleClick.bind(this)

    this.state = {
      email: '',
      password: '',
      error: ''
    }
  }

  componentDidMount () {
    this.emailInputRef.current.focus()
  }

  handleChangeEmail (event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ email: event.target.value })
  }

  handleChangePassword (event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ password: event.target.value })
  }

  handleClick (event: React.MouseEvent) {
    if (!this.state.email || !this.state.password) {
      this.setState({ error: 'Please enter your e-mail address and password' })
      this.emailInputRef.current.focus()
      return
    }

    this.setState({ error: '' })

    const opts = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(this.state)
    }

    fetch('/api/user', opts)
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          this.setState({ error: response.error })
          return
        }

        Router.push('/')
      })
  }

  render () {
    const error = this.state.error

    return (
      <div className='center w-100 w-80-m w-60-l card'>
        <div className='flex flex-column items-center'>
          <div className='mb2 tc'>
            <h1>Please sign in</h1>
          </div>
          <div className='w-100 w5-ns tc'>
            <input
              className='dib w-100'
              type='text'
              placeholder='E-mail address'
              ref={this.emailInputRef}
              onChange={this.handleChangeEmail}
              value={this.state.email}
            />
            <input
              className='dib mt3 w-100'
              type='password'
              placeholder='Password'
              onChange={this.handleChangePassword}
              value={this.state.password}
            />
          </div>
          <div className='mt4 center'>
            <Button label='Sign In' type='primary' onClick={this.handleClick} />
          </div>
          <div className='mt4 center'>
            {error && <span className='bright-red b'>{error}</span>}
          </div>
        </div>
      </div>
    )
  }
}
