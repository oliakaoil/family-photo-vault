import React from "react";
import PhotoModel from "../../models/photo";

interface Props {
  cacheBust?: boolean;
  onClick?: CallableFunction;
  photo: PhotoModel;
  thumb?: boolean;
}

export default function PhotoTile(props: Props) {
  const { cacheBust, onClick, photo, thumb } = props;

  const handleClick = () => {
    onClick && onClick(photo);
  };

  let imgSrc = `/api/photos?i=${photo.id}&stream=1`;

  if (thumb) {
    imgSrc = `${imgSrc}&thumb=1`;
  }

  if (cacheBust) {
    imgSrc = `${imgSrc}&cb=${Date.now()}`;
  }

  return (
    <div className="ma1">
      <img
        src={imgSrc}
        alt={photo.filename}
        onClick={handleClick}
        className="w-100"
      />
    </div>
  );
}
