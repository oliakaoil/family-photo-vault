import React from "react";
import PhotoTile from "./PhotoTile";
import Router from "next/router";
import { Transition } from "react-transition-group";
import { BsArrowBarDown } from "react-icons/bs";
import PhotoModel from "../../models/photo";
import PhotoPage from "./PhotoPage";

interface Props {
  isAdmin: boolean;
}

interface State {
  lastPhoto: PhotoModel | null;
  limit: number;
  offset: number;
  photo: PhotoModel | null;
  photos: PhotoModel[];
}

export class PhotoFeed extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.viewPhoto = this.viewPhoto.bind(this);
    this.loadMore = this.loadMore.bind(this);

    this.state = {
      lastPhoto: null,
      limit: 100,
      offset: 0,
      photo: null,
      photos: [],
    };
  }

  componentDidMount() {
    this.getPhotos();
  }

  componentDidUpdate() {
    const { lastPhoto, photo } = this.state;
    if (!photo && lastPhoto) {
      document
        .getElementById(`photo-${lastPhoto.id}`)
        ?.scrollIntoView({ block: "start" });
    }
  }

  loadMore(event: React.MouseEvent) {
    this.setState(
      { offset: this.state.offset + this.state.limit },
      this.getPhotos
    );
  }

  getPhotos() {
    const opts = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch(
      `/api/photos?limit=${this.state.limit}&offset=${this.state.offset}`,
      opts
    )
      .then((response) => {
        if (response.status === 403) {
          Router.push("/user/sign-in");
          return;
        }

        return response.json();
      })
      .then((response) => {
        if (response) {
          setTimeout(() => {
            this.setState({ photos: this.state.photos.concat(response) });
          }, 2000);
        }
      });
  }

  viewPhoto(photo: PhotoModel) {
    this.setState({ photo });
  }

  render() {
    const { photo, photos } = this.state;
    const { isAdmin } = this.props;

    if (photo) {
      const transitionStyles: any = {
        entering: { opacity: 1 },
        entered: { opacity: 1 },
        exiting: { opacity: 0 },
        exited: { opacity: 0 },
      };

      return (
        <Transition in={true} timeout={500}>
          {(state: string) => (
            <div className="page-transition" style={transitionStyles[state]}>
              <PhotoPage
                isAdmin={isAdmin}
                photo={photo}
                onBack={(photo: PhotoModel) => {
                  this.setState({ photo: null, lastPhoto: photo });
                }}
              />
            </div>
          )}
        </Transition>
      );
    }

    if (!photos?.length) {
      return (
        <div className="flex items-center justify-center w-100">
          <img src="/images/loading-animation.gif" />
        </div>
      );
    }

    const photoTiles = photos.map((photo, index) => {
      return (
        <div
          className="w-100 w-33-m w-25-l clickable"
          key={photo.id}
          id={`photo-${photo.id}`}
        >
          <PhotoTile photo={photo} onClick={this.viewPhoto} thumb={false} />
        </div>
      );
    });

    const recentPhotos = photoTiles.slice(0, 12);
    const allPhotos = photoTiles.slice(12);

    return (
      <div>
        <div className="b mb2 f3">Recent</div>
        <div className="flex flex-wrap">{recentPhotos}</div>
        <div className="b mb2 f3 mt5">All</div>
        <div className="flex flex-wrap">{allPhotos}</div>
        {allPhotos && (
          <div className="mt3 tc">
            <span className="clickable" onClick={this.loadMore}>
              <span className="dib mr2">Load more</span>
              <span className="dib v-top">
                <BsArrowBarDown />
              </span>
            </span>
          </div>
        )}
      </div>
    );
  }
}
