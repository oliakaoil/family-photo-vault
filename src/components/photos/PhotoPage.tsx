import React, { useState } from "react";
import Moment from "react-moment";
import request from "superagent";
import { useSnackbar } from "react-simple-snackbar";
import PhotoTile from "./PhotoTile";
import { MdRotateRight, MdKeyboardBackspace } from "react-icons/md";
import PhotoModel from "../../models/photo";
import { ErrorOpts, SuccessOpts } from "../../snackbar-options";
import { uniqueId } from "lodash";

interface Props {
  isAdmin: boolean;
  onBack?: CallableFunction;
  photo: PhotoModel;
}

export default function PhotoPage(props: Props) {
  const { isAdmin, onBack, photo } = props;

  const [errorSnackbar] = useSnackbar(ErrorOpts);
  const [successSnackbar] = useSnackbar(SuccessOpts);
  const [photoRefreshId, setPhotoRefreshId] = useState("1");

  const rotatePhoto = (photoId: string) => {
    request
      .get(`/api/photos?rotate=1&i=${photoId}`)
      .end((err: any, res: any) => {
        if (err) {
          console.error(err);
          errorSnackbar(
            "Whoops..that didn't work :-(. Who built this website anyways?"
          );
        }

        successSnackbar("The photo was rotated");
        setPhotoRefreshId(uniqueId());
      });
  };

  return (
    <div>
      {photo && (
        <div>
          <div className="flex mb3">
            <div className="w-50">
              Taken on{" "}
              <Moment date={photo.created} format="MMMM Do YYYY @ h:mma" />
            </div>
            <div className="w-50 flex justify-end">
              {isAdmin && (
                <span
                  className="inline-flex items-center justify-end clickable mr4"
                  onClick={() => rotatePhoto(photo.id)}
                >
                  <MdRotateRight /> <span>Rotate</span>
                </span>
              )}
              {onBack && (
                <span
                  className="inline-flex items-center justify-end clickable"
                  onClick={() => onBack(photo)}
                >
                  <MdKeyboardBackspace /> <span>Back</span>
                </span>
              )}
            </div>
          </div>
          <div className="flex justify-center">
            <PhotoTile
              photo={photo}
              key={`photo-tile-${photoRefreshId}`}
              cacheBust={true}
            />
          </div>
        </div>
      )}
    </div>
  );
}
