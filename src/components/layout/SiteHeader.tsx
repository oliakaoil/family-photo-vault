import React from 'react'
import Link from 'next/link'
import { RiLogoutCircleRLine } from 'react-icons/ri'
import { MdAddAPhoto } from 'react-icons/md'
import Router from 'next/router'

interface Props {
  loggedIn: boolean
  isAdmin: boolean
}

export default function SiteHeader (props: Props) {
  const { isAdmin, loggedIn } = props

  return (
    <header className='pt3 pb3'>
      <div className='w-90 center flex items-start'>
        <div className='w-50'>
          <Link href='/'>
            <a>Gould Family Photos</a>
          </Link>
        </div>
        <div className='tr w-50'>
          {loggedIn && isAdmin && (
            <Link href='/photo/add'>
              <a className='mr4'>
                Add Photo
                <span className='v-top ml1'>
                  <MdAddAPhoto />
                </span>
              </a>
            </Link>
          )}

          {loggedIn && (
            <span
              className='clickable'
              onClick={() => Router.push('/user/sign-in')}
            >
              <span className='v-top ml1'>
                <RiLogoutCircleRLine />
              </span>
            </span>
          )}
        </div>
      </div>
    </header>
  )
}
