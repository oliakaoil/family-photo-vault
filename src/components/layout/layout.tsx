import React from "react";
import Head from "next/head";
import SiteHeader from "./SiteHeader";
import Router from "next/router";
import { useState } from "react";
import { useEffect } from "react";

interface Props {
  children: any;
  pageTitle: string;
}

export default function Layout(props: Props) {
  const [loggedIn, setLoggedIn] = useState<boolean>(false);
  const [isAdmin, setIsAdmin] = useState<boolean>(false);

  const handleLoginChange = (loggedIn: boolean, isAdmin?: boolean) => {
    isAdmin = Boolean(isAdmin);

    setLoggedIn(loggedIn);
    setIsAdmin(isAdmin);

    if (!loggedIn) {
      const opts = {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
      };

      fetch("/api/user", opts);
      return;
    }
  };

  const checkLoginStatus = () => {
    const opts = {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    };

    fetch("/api/user", opts)
      .then((response) => response.json())
      .then((response) => {
        if (!response?.data?.email) {
          Router.push("/user/sign-in");
          return;
        }

        handleLoginChange(true, response.data.isAdmin);
      });
  };

  useEffect(() => {
    checkLoginStatus();
  }, []);

  const { children, pageTitle } = props;

  return (
    <>
      <Head>
        <title>{pageTitle ? pageTitle : "Family Photo Vault"}</title>
        <link rel="icon" href="/favicon.ico" />
        <meta content="text/html; charset=utf-8" httpEquiv="content-type" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;500;700&display=swap"
          rel="stylesheet"
        ></link>
      </Head>
      <SiteHeader isAdmin={isAdmin} loggedIn={loggedIn} />
      <main className="w-100">
        <div className="w-90 center pt4 pb4">
          {React.cloneElement(children, { isAdmin })}
        </div>
      </main>
      <footer></footer>
    </>
  );
}
