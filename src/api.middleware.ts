import { NextApiRequest, NextApiResponse } from "next";

export interface IApiMiddleware {
    all(req: NextApiRequest, res: NextApiResponse): void
}

export class ApiMiddleware implements IApiMiddleware {
    public all(req: NextApiRequest, res: NextApiResponse) {
        // add helper methods?
    }
}