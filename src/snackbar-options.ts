export const ErrorOpts = {
  style: {
    backgroundColor: "#ac0202",
    border: "1px solid #de0606",
    color: "#ffffff",
  },
  closeStyle: {
    color: "#ffffff",
  },
};

export const SuccessOpts = {
  style: {
    backgroundColor: "#006da6",
    border: "1px solid #006da6",
    color: "#ffffff",
  },
  closeStyle: {
    color: "#ffffff",
  },
};
