const glob = require("glob");
const path = require("path");
const fs = require("fs");
const md5 = require("md5");
const uniqid = require("uniqid");

const photos = [];

glob("./src/*.jpg", null, (err, files) => {
  if (err) throw new Error(err);

  files.forEach((file, index) => {
    if (file.includes("thumb") || file.includes("deleted/")) return;

    const stats = fs.statSync(file);

    console.log(`${file}-${stats.mtime}`);

    const id = md5(`${uniqid()}${new Date().getTime()}`);

    const photo = {
      id,
      filename: path.basename(file),
      filesize: stats.size,
      created: stats.mtime,
    };

    photos.push(photo);
  });

  photos.sort((a, b) => {
    const aComp = a.created;
    const bComp = b.created;
    if (aComp === bComp) {
      return 0;
    }
    return aComp > bComp ? -1 : 1;
  });

  fs.writeFileSync("../data/photodb.json", JSON.stringify({ photos }, null, 2));
});
