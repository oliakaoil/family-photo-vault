module.exports = {
  publicRuntimeConfig: {
    PHOTODB_FILEPATH: process.env.PHOTODB_FILEPATH,
    PHOTO_FILEPATH: process.env.PHOTO_FILEPATH
  },
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    return config
  },
  webpackDevMiddleware: config => {
    return config
  }
}
